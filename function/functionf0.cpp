#include "functionf0.h"
#include "ui_functionf0.h"

#include <QCanBus>
#include <QCanBusFrame>
#include <QDebug>

enum {
    MinEmergency = 0,
    MaxEmergency = 100
};

//F00返回的信息字段表
enum codes {
    c00, c01, c02, c03, c04, d00,
    unknown
};

codes hashit(QString inString) {
    if (inString == "c00") return c00;
    if (inString == "c01") return c01;
    if (inString == "c02") return c02;
    if (inString == "c03") return c03;
    if (inString == "c04") return c04;
    if (inString == "d00") return d00;
    else return unknown;
}

/**
 * @brief 初始化当前Function00的界面、完成对不同事件的连接。
 *
 * @param parent
 */
FunctionF0::FunctionF0(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FunctionF0)
{
    ui->setupUi(this);

    ui->powerEmergency->setRange(0, 100);
    ui->powerEmergency->setSingleStep(1);

    initActionConnections();
    reflectParameters();
}

/**
 * @brief 设定为在F00上更改任意操作按钮时会在当前的后台中产生的事件连接。
 */
void FunctionF0::initActionConnections() {

    connect(ui->powerEmergency, &QSpinBox::textChanged, this, &FunctionF0::updateParameters);
    connect(ui->power24V, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);

    connect(ui->startupCheckSupply, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);
    connect(ui->startupCheckPot, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);

    connect(ui->checkCurrentPlausibility, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);
    connect(ui->checkSupplyPlausibility, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);
    connect(ui->checkInternal5V, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);
    connect(ui->checkExternal5V, &QCheckBox::stateChanged, this, &FunctionF0::updateParameters);

    connect(this, &FunctionF0::parameterChanged, this, &FunctionF0::reflectParameters);
}

FunctionF0::~FunctionF0()
{
    delete ui;
}

FunctionF0::Parameters FunctionF0::parameters() const {
    return m_parameters;
}

/**
 * @brief 接收信息包的Slot函数。将必要的设置应用到F00参数表中并予以保存。
 * @param payloads
 */
void FunctionF0::parameterReceived(QVector<QString> payloads) {
    for (int i = 0; i < payloads.count(); i++) {
        //qDebug() << payloads.at(i);
        QString key = payloads.at(i).simplified().split(':')[0];
        if (payloads.at(i).simplified().split(':').count() > 1) {
            QString val = payloads.at(i).simplified().split(':')[1];
            switch (hashit(key)) {
                case c00:
                    break;
                case c01:
                    break;
                case c02:
                    int actual_val;
                    actual_val = val.toInt(nullptr, 16);
                    if (actual_val / 128 >= 1) {
                        //当数值大于128时，即当前24V开关打开
                        m_parameters.supply24V = true;
                        emit parameterChanged();
                        actual_val -= 128;
                    } else {
                        m_parameters.supply24V = false;
                        emit parameterChanged();
                    }

                    if (actual_val / 64 >= 1) {
                        //此时当数值大于64时，即当前检查电压真实性为打开
                        m_parameters.checkSupply = true;
                        emit parameterChanged();
                        actual_val -= 64;
                    } else {
                        m_parameters.checkSupply = false;
                        emit parameterChanged();
                    }

                    if (actual_val / 32 >= 1) {
                        //此时当数值大于32时，即启动时检查Pot电压打开
                        m_parameters.startupCheckPot = true;
                        emit parameterChanged();
                        actual_val -= 32;
                    } else {
                        m_parameters.startupCheckPot = false;
                        emit parameterChanged();
                    }

                    if (actual_val / 16 >= 1) {
                        //此时当数值大于16时，即启动时检查供电电压打开
                        m_parameters.startupCheckSupply = true;
                        emit parameterChanged();
                        actual_val -= 16;
                    } else {
                        m_parameters.startupCheckSupply = false;
                        emit parameterChanged();
                    }

                    if (actual_val / 8 >= 1) {
                        //此时当数值大于8时，即检查外部5V电压打开
                        m_parameters.checkExternal = true;
                        emit parameterChanged();
                        actual_val -= 8;
                    } else {
                        m_parameters.checkExternal = false;
                        emit parameterChanged();
                    }

                    if (actual_val / 4 >= 1) {
                        //此时当数值大于4时，即检查内部5V电压打开
                        m_parameters.checkInternal = true;
                        emit parameterChanged();
                        actual_val -= 4;
                    } else {
                        m_parameters.checkInternal = false;
                        emit parameterChanged();
                    }

                    if (actual_val / 2 >= 1) {
                        actual_val -= 2;
                    }

                    if (actual_val / 1 >= 1) {
                        //此时当数值为1，即最后一个开关，检查电流真实性为打开。
                        m_parameters.checkCurrent = true;
                        emit parameterChanged();
                    } else {
                        m_parameters.checkCurrent = false;
                        emit parameterChanged();
                    }
                    break;
                case c03:
                    break;
                case c04:
                    break;
                case d00:
                    m_parameters.emergencyPercentage = val.toInt(nullptr, 16) * 100 / 4096;
                    emit parameterChanged();
                    break;
                case unknown:
                    break;
            }
        }
    }
}

/**
 * @brief 将当前保存的参数数据显示在界面上
 */
void FunctionF0::reflectParameters() {
    //将当前设置在Parameters上的参数显示在配置界面中
    ui->powerEmergency->setValue(m_parameters.emergencyPercentage);
    ui->power24V->setChecked(m_parameters.supply24V);

    ui->startupCheckSupply->setChecked(m_parameters.startupCheckSupply);
    ui->startupCheckPot->setChecked(m_parameters.startupCheckPot);

    ui->checkCurrentPlausibility->setChecked(m_parameters.checkCurrent);
    ui->checkSupplyPlausibility->setChecked(m_parameters.checkSupply);
    ui->checkInternal5V->setChecked(m_parameters.checkInternal);
    ui->checkExternal5V->setChecked(m_parameters.checkExternal);
}

/**
 * @brief 将从界面上更改的参数数据保存到参数表中
 */
void FunctionF0::updateParameters() {
    //将配置界面中设置的参数更新到当前的Parameters中
    m_parameters.emergencyPercentage = ui->powerEmergency->value();
    m_parameters.supply24V = ui->power24V->isChecked();

    m_parameters.startupCheckSupply = ui->startupCheckSupply->isChecked();
    m_parameters.startupCheckPot = ui->startupCheckPot->isChecked();

    m_parameters.checkCurrent = ui->checkCurrentPlausibility->isChecked();
    m_parameters.checkSupply = ui->checkSupplyPlausibility->isChecked();
    m_parameters.checkInternal = ui->checkInternal5V->isChecked();
    m_parameters.checkExternal = ui->checkExternal5V->isChecked();

    /*
    qDebug() << "params:" <<
                "emergency%：" << m_parameters.emergencyPercentage <<
                "24V：" << m_parameters.supply24V <<
                "startupCheck-Supply：" << m_parameters.startupCheckSupply <<
                "startupCheck-Pot:" << m_parameters.startupCheckPot <<
                "checkCurrent:" << m_parameters.checkCurrent <<
                "checkSupply:" << m_parameters.checkSupply <<
                "checkInternal5V:" << m_parameters.checkInternal <<
                "checkExternal5V:" << m_parameters.checkExternal;
    */
}

/**
 * @brief 当按下读取按钮时执行的操作。通过0x18FFFF30字段发送R00信息。
 */
void FunctionF0::on_buttonRead_clicked()
{
    const uint frameId = 0x18FFFF30;
    const QByteArray payload = "R00";

    QCanBusFrame frame = QCanBusFrame(frameId, payload);
    frame.setExtendedFrameFormat(true);
    emit sendFrame(frame);
}

/**
 * @brief 当按下更新按钮时执行的操作。将当前参数表中的数据翻译成EHA识别的字段并逐个发送。
 */
void FunctionF0::on_buttonUpdate_clicked()
{
    //点击更新之后，会将当前在设置表中的数据发送到仪器中。
    const uint frameId = 0x18FFFF30;

    QCanBusFrame frame = QCanBusFrame(frameId, "");
    frame.setExtendedFrameFormat(true);

    //第一frame：F00，打开F00输入
    frame.setPayload("F00");
    emit sendFrame(frame);

    //第二frame：C00数值，暂时未知有没有变化
    frame.setPayload("C00.54");
    emit sendFrame(frame);

    //第三frame：C01数值，暂时未知有没有变化
    frame.setPayload("C01.4D");
    emit sendFrame(frame);

    //第四frame:C02数值，根据当前设置组合出来。
    frame.setPayload(makeC02().toUtf8());
    emit sendFrame(frame);

    //第五frame:D00数值，读取当前设置。
    frame.setPayload("D00." + QString::number(m_parameters.emergencyPercentage * 4096 / 100, 16).rightJustified(4,'0').toUpper().toUtf8());
    emit sendFrame(frame);

    //最后frame：结束。
    frame.setPayload("S:");
    emit sendFrame(frame);

}

/**
 * @brief 辅助函数，将当前参数表中的设定转换为EHA识别的C2字段。
 * @return 设置用的C2字段
 */
QString FunctionF0::makeC02() {
    int c02_value = 0;
    if (m_parameters.checkCurrent) c02_value += 1;
    if (m_parameters.checkInternal) c02_value += 4;
    if (m_parameters.checkExternal) c02_value += 8;
    if (m_parameters.startupCheckSupply) c02_value += 16;
    if (m_parameters.startupCheckPot) c02_value += 32;
    if (m_parameters.checkSupply) c02_value += 64;
    if (m_parameters.supply24V) c02_value += 128;
    return "C02." + QString::number(c02_value, 16).toUpper();
}
