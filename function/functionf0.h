#ifndef FUNCTIONF0_H
#define FUNCTIONF0_H

#include <QWidget>
#include <QDialog>
#include <QCanBusFrame>
#include <QValidator>

namespace Ui {
class FunctionF0;
}

class FunctionF0 : public QWidget
{
    Q_OBJECT

public:
    // 第一部分，创建用于保存F00函数的参数struct
    struct Parameters {
        int emergencyPercentage = 50;
        bool supply24V = true;
        bool startupCheckSupply = true;
        bool startupCheckPot = true;
        bool checkCurrent = false;
        bool checkSupply = false;
        bool checkInternal = true;
        bool checkExternal = false;
    };

    //创建本F00的constructur与destructor，这些由Qt自动生成
    explicit FunctionF0(QWidget *parent = nullptr);
    ~FunctionF0();

    //公开函数，可以直接调用当前F00上设置的参数。之后可以用于批量保存当前载入的参数。
    Parameters parameters() const;

//从其它函数上发送数据到该函数，就使用Slots
public slots:
    void parameterReceived(QVector<QString>);

//同时从该Widget发送数据到其它元件的Slot上，需要提供Signals
signals:
    void sendFrame(const QCanBusFrame &frame);
    void parameterChanged();

//在该元件本身的Slot。针对按钮用的事件都放在这里
private slots:
    void on_buttonRead_clicked();

//只有本元件才会用到的一些函数数据。
    void on_buttonUpdate_clicked();

private:
    Ui::FunctionF0 *ui;
    Parameters m_parameters;

    void reflectParameters();
    void updateParameters();
    void initActionConnections();

    QString makeC02();
};

#endif // FUNCTIONF0_H
