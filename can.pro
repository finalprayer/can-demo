QT += serialbus widgets
requires(qtConfig(combobox))

TARGET = can
TEMPLATE = app

SOURCES += \
    interface/bitratebox.cpp \
    interface/connectdialog.cpp \
    function/functionf0.cpp \
    main.cpp \
    main/mainwindow.cpp \
    main/sendframebox.cpp

HEADERS += \
    interface/bitratebox.h \
    interface/connectdialog.h \
    function/functionf0.h \
    main/mainwindow.h \
    main/sendframebox.h

FORMS   += main/mainwindow.ui \
    interface/connectdialog.ui \
    function/functionf0.ui \
    main/sendframebox.ui

RESOURCES += can.qrc

target.path = ../build/serialbus/can
INSTALLS += target
